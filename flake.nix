{
  description =
    "Lazy Depth First search and Linear Graph Algorithms in Haskell";
  inputs = {
    np.url = "github:nixos/nixpkgs?ref=master";
    fu.url = "github:numtide/flake-utils?ref=master";
    hls.url = "github:haskell/haskell-language-server?ref=master";
  };
  outputs = { self, np, fu, hls }:
    with np.lib;
    with fu.lib;
    eachSystem [ "x86_64-linux" ] (system:
      let
        version =
          "${substring 0 8 self.lastModifiedDate}.${self.shortRev or "dirty"}";
        config = { };
        overlay = final: _:
          with self;
          with haskell.lib;
          with final.haskellPackages.extend (_: _: { }); {
            ldfs = (callCabal2nix "ldfs" ./. { }).overrideAttrs (o: {
              version = "${o.version}.${version}";
              doCheck = true;
            });
          };
        overlays = [ overlay hls.overlay ];
      in with (import np { inherit config system overlays; }); rec {
        packages = flattenTree (recurseIntoAttrs { inherit ldfs; });
        defaultPackage = packages.ldfs;
        inherit overlays;
        devShell = with haskellPackages;
          shellFor {
            packages = _: [ ];
            buildInputs = [ cabal-install haskell-language-server ghc ];
          };
      });
}
