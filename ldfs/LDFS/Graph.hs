{-# LANGUAGE ViewPatterns #-}

-- |
module LDFS.Graph where

import Data.Array
import Optics hiding (indices)

newtype Graph v = G {_graph :: Array v [v]}
  deriving (Show)

makeLenses ''Graph

mk :: Ix v => (v, v) -> [(v, v)] -> Graph v
mk b = G . accumArray (:>) [] b

-- >>> mk ('a','j') [('a','b'), ('a','f'), ('b','c'), ('b','e'),('c','a'),('c','d'),('e','d'),('g','h'),('g','j'),('h','f'),('h','i'),('h','j')]
-- G {_graph = array ('a','j') [('a',"bf"),('b',"ce"),('c',"ad"),('d',""),('e',"d"),('f',""),('g',"hj"),('h',"fij"),('i',""),('j',"")]}

vertices :: Ix v => Graph v -> [v]
vertices = view (graph % to indices)

edges :: Ix v => Graph v -> [(v, v)]
edges g@(view graph -> garr) = do v <- indices garr; v' <- garr ! v; pure (v, v')

outdeg :: (Ix v) => Graph v -> Array v Int
outdeg g@(view graph -> garr) = array (bounds garr) $ indices garr <&> \v -> (v, garr ! v & length)

indeg :: (Ix v) => Graph v -> Array v Int
indeg = outdeg . transpose

rev :: Ix v => Graph v -> [(v, v)]
rev = reverse . edges

transpose :: Ix v => Graph v -> Graph v
transpose g = mk (g ^. (graph % to bounds)) (rev g)
