module LDFS.Set where

import Control.Monad.ST
import Data.Array.ST

-- | mutable set
type Set s v = STUArray s v Bool

mk :: Ix v => (v, v) -> ST s (Set s v)
mk = (`newArray` False)

with :: Ix v => Set s v -> v -> ST s a -> ST s a -> ST s a
with s v t f =
  readArray s v >>= \case
    True -> t
    _ -> f

set :: Ix v => Set s v -> v -> ST s ()
set s v = writeArray s v True
